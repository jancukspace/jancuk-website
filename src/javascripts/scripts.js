import $ from 'jquery';
import Swiper from 'swiper';
import lottie from 'lottie-web';
import animationData from '../assets/lottie/data.json';
import community from '../images/community page.png';
import wiz from '../images/wiz.png';
import plugin from '../images/plugin.png';
import '../images/background-sectionE.png';
import '../images/next.png';
import '../images/next-hover.png';
import '../images/back.png';
import '../images/back-hover.png';

const images = {
  community,
  wiz,
  plugin,
};

window.jQuery = $;
window.$ = $;

$(document).ready(() => {
  $('.form_error').hide();
  $('.form_success').hide();

  const goToStudio = (eventName) => {
    if (window.analytics) {
      window.analytics.track(eventName, {
        description: `Use clicked on ${eventName}`,
        event: `Click on ${eventName}`,
      });
    }
    window.open('https://studio.jancuk.space', '_blank', 'noopener');
  };

  const goToPost = (path) => {
    window.open(path, '_blank', 'noopener');
  };

  lottie.loadAnimation({
    container: document.getElementById('lottie'),
    animationData,
    renderer: 'svg',
    loop: true,
  });

  const mySwiper = new Swiper('.swiper-container', {
    direction: 'horizontal',
    loop: true,
    setWrapperSize: true,
    pagination: {
      el: '.swiper-pagination',
    },
    autoplay: {
      delay: 5000,
    },
    initialSlide: 0,
    slidesPerView: 1,
    spaceBetween: 20,
  });

  const myCommunitiesSwiper = new Swiper('.communities-swiper-container', {
    direction: 'horizontal',
    loop: true,
    setWrapperSize: true,
    pagination: {
      el: '.communities__pagination',
    },
    autoplay: {
      delay: 5000,
    },
    initialSlide: 0,
    slidesPerView: 1,
    navigation: {
      nextEl: '.communities__button--next',
      prevEl: '.communities__button--prev',
    },
  });

  mySwiper.on('slideChange', () => { });
  myCommunitiesSwiper.on('slideChange', () => { });

  myCommunitiesSwiper.on('transitionStart', () => {
    $('#communities > img').removeClass('opaque');
    $('#communities > img').eq(myCommunitiesSwiper.realIndex).addClass('opaque');
  });

  function reset() {
    document.querySelectorAll('.carousel__indicator').forEach((elem) => {
      elem.classList.remove('carousel__indicator--active');
    });
    document.querySelectorAll('.left-line').forEach((elem) => {
      elem.classList.remove('left-line--active');
    });
  }

  $('#toggle').click(() => {
    $('#toggle').toggleClass('active');
    $('.header__nav').toggleClass('header__nav__open');
  });

  document.querySelectorAll('.left-line').forEach((elem, index) => {
    elem.addEventListener('mouseover', () => {
      const { dataset: { image } } = elem;
      $('#carouselImage').attr('src', images[image]);
      reset();
      elem.classList.add('left-line--active');
      $(`#indicator${(index + 1)}`).addClass('carousel__indicator--active');
    });
  });

  document.querySelectorAll('.goToStudioListener').forEach((elem) => {
    elem.addEventListener('click', () => {
      const { dataset: { event } } = elem;
      goToStudio(event);
    });
  });

  document.querySelectorAll('.goToPostListener').forEach((elem) => {
    elem.addEventListener('click', () => {
      const { dataset: { link } } = elem;
      goToPost(link);
    });
  });

  $('#myForm').on('submit', (e) => {
    e.preventDefault();
    const email = $('#email').val();
    if (email) {
      const user = {
        accountAddress: 'Jancuk.Space',
        email,
        provider: 'HDWallet',
        subscribe: false,
        source: 'Jancuk.Space',
        displayName: 'Jancuk.Space',
      };
      $.ajax({
        url: 'https://studio.jancuk.space/api/v2/users',
        method: 'POST',
        data: user,
        success: () => {
          $('.form_success').show().fadeToggle(5000);
          $('#email').val('');
        },
        error: () => {
          $('.form_error').show().fadeToggle(5000);
          $('#email').val('');
        },
      });
    }
  });
});
